This is a mesh processing program containing basic mesh processing algorithms such as 
	mesh subdivision (butterfly and loop), 
	mesh simplification (QSlim and vertex removal), and 
	mesh deformation (laplacian surface editing, ARAP, and subspace nonlinear deformer). 
They are all developed as the homework and course project for CPSC 524 - Geometric Modeling 
	http://www.cs.ubc.ca/~sheffa/dgp/ 
at UBC. 
The original framework is Cartel 1.4 
	http://www.cs.ubc.ca/labs/imager/tr/2014/CartelModeling/Cartel_website/
developed by the TAs Russell Gillette, Darcy Harisson, and Giorgio Gori.

Intel TBB is needed for compiling. For more configuration issues, please see the documentations of
Cartel, it nearly take no time to set the environment.

Keyboard control manual with detailed functionality introduction:
	Mesh Subdivision:
		Key 1 - one step of butterfly subdivision (only support closed manifold meshes)
		Key 2 - one step of loop subdivision (support manifold meshes possibly with boundaries)

	Mesh Simplification:
		Key 3 - QSlim edge collapse mesh simplification
		    	Input 0 to see the most extreme valid simplification result
		    	Input (0.0, 1.0) to see simplification result with the input fraction of the original
		    	triangles left
		    	Input integers to see simplification result with the input number of triangles left
		Key 4 - QSlim edge collapse mesh simplification, collapse one edge (the one introducing smallest error)
		
		Key 5 - Vertex removal mesh simplification
		    	Input 0 to see the most extreme valid simplification result
		    	Input (0.0, 1.0) to see simplification result with the input fraction of the original
		    	triangles left
		    	Input integers to see simplification result with the input number of triangles left
		Key 6 - Vertex removal mesh simplification, remove one vertex (the one introducing smallest
		error)

		*For Key 3 - Key 6 operations, once one operation is done, the visualization of the 10
		candidates introducing smallest errors will be rendered with purple color showing the next
		candidate, and others are colored from red to green with error increases, where for
			edge collapse, the two incident triangles of the edge is colored, and for
			vertex removal, the umbrella of the vertex is colored.
	
	Mesh Deformation:
		Key 7 - Mesh Deformation with arap(default) or vertex based laplacian solver
				Use 8 to toggle arap or vertex based laplacian solver
				First select a group of vertices and press 7 to make them fixed vertices (they will
				be colored blue)
				Then select another group of vertices and press 7 to make them moving vertices (they
				will be colored red)
				Then just drag the mouse and the moving vertices will be translated to trigger the
				deformation at the same time
				If you got a wonderful shape and want to keep it, first press x to set the current
				shape as initial, and then 
					press z if you want to set new position constraints, then you can start from here
					to do further deformations
				Press z if you want to get back to the original shape and change position constraints

		Nonlinear Deformation:
			For subspace solve, first press F1 to load control mesh, and then use the same UI as
			above to select vertices to fix or move,
				then press F2 to set moving vector that displaces those moving vertices, and then
				press F3 to start Gauss-Newton iterations.
			Screenshots of result after each iteration will be taken and saved in the working
			directory, a file saving the objective functions
				will also be written into disk.
			For full space solve, just start selecting the vertices without loading the control mesh.


NOTE: Once either one of the mesh simplification operations are conducted, all the other operations 
are not supported anymore, only the 2 same type of simplification operations could be conducted again
interchangeably. To test other operations, just reload the same model (or other models since loading 
model will reinitialize the mesh data structure).
	This is because the mesh simplification operations are lazy deleting the vertices and half edges.
	It's possible to support other types of operations after simplification if a rearrangement of the 
	mesh data structures is provided.






Following is the original readme file of Cartel:

# Cartel
Mesh Viewing and Modification Tool

## Overview 
Cartel is a mesh visualization and modification tool designed for simplicity and clarity. It is built on
top of a basic OpenGL 3.3 environment using the GLFW windowing system, and enables complex
rendering effects through the easy use of shaders. Deployment and compilation are
simple due to the use of few, and common, dependencies.

## About
Cartel was developed in early 2014 by Russell Gillette and Darcy Harisson with the sole purpose of 
never having to use Meshlab ever again. It was developped from a rendering framework that Russell 
had been working on across many projects and a half-edge data structure that Darcy had whipped together 
to try and use Maya instead. Where meshlab offered an exceedingly complicated interface, exceedingly 
complicated dependancies, and minimal documentation, we have strived to offer a clean interface with 
only the basic modeling accoutrements. There is no QT, and model modification code is added directly to 
the model representation internally. Furthermore we enforce mesh manifoldness at all times. 
You will curse the asserts that this causes. But trust me, having lots of problems as soon as you do 
something stupid is better than having sparadic problems long after with no clue as to why. --Russell

This fork focuses on many usability improvements, both during setup, while coding, and 
while using the program:
* Improved Unix support with revamped Makefile. It *should* work on OSX just by installing few 
common dependencies. For more info, check the Makefile directly.
* Cleanup and code fixes
* Added ImGui as a user interface to improve usability such as loading models and changing view modes. 
The developer is also encouraged to add their own functionality in the menu bar, and can potentially 
implement controls such as sliders, inputs, etc to change parameters without modifying the source code.

--Giorgio

## Dependencies
The libraries included with the build are for VisualStudio 2012 or later, and the source code includes
some (though not many) C++ 11 operations, and thus will not work with earlier compilers. That
said, there is nothing OS specific within the code, and alternative libraries are easily acquired at the
below links.

### GLEW 
GLEW is the extension wrangler library for OpenGL that allows the use of newer OpenGL functionality
on computers that do not support it. You will never have to do anything using this library,
its just included, and thus worth mentioning. Its home webpage is here:
http://glew.sourceforge.net/

### GLM
GLM is the open gl math extensions and provides a lot of useful functionality when interfacing with
GLSL, the OpenGL shader language. This library provides types, and functions on those types
used SOLELY FOR RENDERING. This is important, because the glm types do not oer double
precision, and do not interface easily with Eigen. Use glm only for visual eects or when passing
data to the GPU in main.
http://glm.g-truc.net/0.9.5/index.html

### GLFW
GLFW is the windowing system used by Cartel. It is a common replacement for GLUT, and was
chosen due to its nicer rendering loop and newer interface. This windowing library is cross-platform,
allowing the deployment of this system across OSes, but does, at the time of writing this, make
setting up this project on a linux system with integrated graphics dificult.
http://www.glfw.org/

### Eigen
Eigen is the back-end linear-algebra library used by Cartel, and the largest source of complexity
within the project. This library makes heavy use of templates, but is well documented. Documentation
can found here: http://eigen.tuxfamily.org/index.php?title=Main_Page

### ImGui
ImGui is the user interface library used by Cartel. It should never create dependency issues as it's small 
and included in the project. For more information on the library check its page: https://github.com/ocornut/imgui
